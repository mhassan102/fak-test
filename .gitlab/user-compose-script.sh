#!/bin/bash

export DOCKER_COMMIT=$CI_COMMIT_SHORT_SHA
echo $DOCKER_COMMIT
export NETWORK=testnet
export PROXY_PORT=3000
cd user-mgmt && docker-compose up -d

set -euxo pipefail

HOST=docker
PORT=${PROXY_PORT}
URL=http://${HOST}:${PROXY_PORT}

remaining=3600
pause=30

cmd=( curl --netrc --fail --silent --show-error --head "$URL" )
echo "Waiting for postgrest to come up..."

while (( remaining > 0 )); do
  if "${cmd[@]}"; then
    echo 'UP!'
    curl -v -X GET "$URL" > user-postgrest.yaml
    exit 0
  fi
  for container in proxy postgres postgrest ; do
   if [[ "$(docker inspect -f '{{.State.Status}}' "$(docker-compose ps -q ${container})")" == "exited" ]] ; then
     echo 'Container exited unexpectedly!'
     exit 1
   fi
  done
  (( remaining -= pause ))
  sleep "${pause}s"
done

echo 'Time is over'
exit 1

